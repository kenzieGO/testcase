import 'package:flutter/material.dart';
import 'package:test1/test1.dart';
import 'package:test1/test2.dart';
import 'package:test1/test3.dart';
import 'package:test1/test3a.dart';
import 'package:test1/test3b.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Cases',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(title: 'Test cases',),
        '/test1': (context) => test1(),
        '/test2': (context) => test2(),
        '/test3': (context) => test3(),
        '/test3a': (context) => test3a(),
        '/test3b': (context) => test3b(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    ButtonStyle style_button=TextButton.styleFrom(
      primary: Colors.white,
      backgroundColor: Colors.blue,
      onSurface: Colors.grey,
      padding:  const EdgeInsets.all( 20.0),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            TextButton(
              style: style_button,
              onPressed: () {
                Navigator.pushNamed(context, '/test1');
              },
              child: Text('Test 1'),
            ),

            SizedBox(height: 10),
            TextButton(
              style: style_button,
              onPressed: () {
                Navigator.pushNamed(context, '/test2');
              },
              child: const Text('Test 2'),
            ),

            SizedBox(height: 10),
            TextButton(
              style: style_button,
              onPressed: () {
                Navigator.pushNamed(context, '/test3');
              },
              child: Text('Test 3'),
            ),


          ],

        ),
      ),
     );
  }
}
