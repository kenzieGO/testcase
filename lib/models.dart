class profilePic {

  profilePic({required this.host,required this.path, required this.filename});

  final String host;
  final String path;
  final String filename;

  factory profilePic.fromJson(Map<String, dynamic> json) {
    String filenamej=json['filename'].toString();
    String pathj=json['path'].toString();
    String hostj=json['host'].toString();

    return profilePic(host:hostj,path:pathj,filename:filenamej);
  }

}

class args {

  final String title;
  final String message;

  args(this.title, this.message);

}

