import 'package:flutter/material.dart';


var result_skipped_array='';
var rawArray;

class test1 extends StatefulWidget{
  @override
  _test1State createState() => new _test1State();

}


class _test1State extends State<test1>{

  void setItem(result){
    setState(() {
      result_skipped_array=result;
    });
  }

  ButtonStyle style_button=TextButton.styleFrom(
    primary: Colors.white,
    backgroundColor: Colors.blue,
    onSurface: Colors.grey,
    padding:  const EdgeInsets.all( 20.0),
  );

  @override
  Widget build(BuildContext context) {

    return  Scaffold(
      appBar: AppBar(
        title: Text('Test 1'),
      ),
      body:Center(
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:  <Widget>[

             Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: TextField(
                onChanged: (text) async {
                  rawArray=text;
                },
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter array number(int) comma separated',
                ),
              ),
            ),

            SizedBox(height: 10),
            Center(
              child:TextButton(
                style: style_button,

                onPressed: () {
                  getSkippedIntegerNumber();
                },
                child: Text('Calculate'),
              ),
            ),


            SizedBox(height: 30),

            Center(
              child:Text(
                  result_skipped_array,
                  textAlign: TextAlign.center,

                  style: new TextStyle(
                    fontSize: 20.0,
                    color: Colors.blue,
                  ),
                ),
            ),

          ],


        ),
      )
    );
  }

  void getSkippedIntegerNumber() async {
    var text=rawArray.toString();
    text=text.replaceAll(' ', '');
    text=text.replaceAll(", \$", "");

    var SkippedNumber=<int>[];
    var rawNumbersStringArray =text.split(',');

    var rawNumbersArray = rawNumbersStringArray.map(int.parse).toList();//convert list to integer
    rawNumbersArray=rawNumbersArray.toSet().toList();//remove the same value array item
    rawNumbersArray.sort();//sort incrementlly

    var currentNumber=0;
    for (int sNumber in rawNumbersArray) {
      //if(sNumber==',' || sNumber.trim()=='')continue;

      //print(sNumber.toString());
      if(currentNumber!=0 && sNumber>currentNumber+1){//cek present number in loops is properly increment
        for(var s=currentNumber;s<(sNumber-1);s++){//if not add the number gap between
          SkippedNumber.add(s+1);//adding skipped number to array
        }
      }

      currentNumber=sNumber;
    }

    SkippedNumber.sort();// it s just in case array not incemented
    setItem(SkippedNumber.join(','));
  }

}