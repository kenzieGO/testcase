import 'package:flutter/material.dart';


var passed_status='',rounded_status='';
var nilai='0';

class test2 extends StatefulWidget{
  @override
  _test2State createState() => new _test2State();
}

class _test2State extends State<test2>{
  @override
  Widget build(BuildContext context) {

    const  _buttonColorWhite = Colors.white60;
    const _buttonHighlightColor=Colors.white10;
    const _buttonColorGrey=Colors.white12;
    const _primarySwatchColor=Colors.blue;

    double _buttonFontSize=18;
    double  _padding = 10;

    return  Scaffold(
        appBar: AppBar(
          title: Text('Test 2'),
        ),
        body:Center(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children:  <Widget>[

              Expanded(
                key: Key("expanded_bagian_atas"),
                flex: 1,
                child: Container(
                  key: Key("expanded_container_bagian_atas"),
                  width: double.infinity,
                  height: double.infinity,
                  padding: EdgeInsets.all(_padding),
                  child: Column(
                    children: <Widget>[

                      Text(
                        nilai.toString(),
                        style: Theme.of(context).textTheme.headline1,
                        maxLines: 1,
                      ),

                      Text(
                        '$rounded_status',
                        style: Theme.of(context).textTheme.headline6,
                        maxLines: 1,
                      ),
                      Text(
                        '$passed_status',
                        style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.blue,
                        ),
                        maxLines: 1,
                      ),

                    ],

                ),
              ),
              ),



              Expanded(
                key: Key("expanded_bagian_bawah"),
                flex: 1,
                child: Column(
                  key: Key("expanded_column_bagian_bawah"),
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "Clear",
                                style: TextStyle(
                                    color: _primarySwatchColor,
                                    fontSize: _buttonFontSize),
                              ),
                              onPressed: () {
                                doCalculate('-1');
                              },
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "0",
                                  style: TextStyle(
                                      color: _primarySwatchColor,
                                      fontSize: _buttonFontSize
                                    ),
                                  ),
                              onPressed: () {
                                doCalculate('0');
                              },
                            ),

                            ),


                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "1",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('1');
                              },
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "2",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('2');
                              },
                            ),
                          ),

                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "3",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('3');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "4",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('4');
                              },
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "5",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('5');
                              },
                            ),
                          ),

                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "6",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('6');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "7",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('7');
                              },
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "8",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('8');
                              },
                            ),
                          ),

                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: _buttonColorWhite,
                              highlightColor: _buttonHighlightColor,
                              child: Text(
                                "9",
                                style: TextStyle(
                                  color: _primarySwatchColor,
                                  fontSize: _buttonFontSize,
                                ),
                              ),
                              onPressed: () {
                                doCalculate('9');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )

            ],


          ),
        )
    );
  }

  void doCalculate(String str) => setState(() {

      if(str=='-1'){//clear clicked
        passed_status='';
        rounded_status='';
        nilai='0';
        return ;
      }

      if(str=='0' && nilai=='0')//if zero clicked firstly
         return;

      if(nilai.length==3)return;//limit 3 digit
      if(nilai.length==2 && str!='0'|| int.parse(nilai)>10 ) {  // limit 100
          return;
      }

      if(nilai=='0') {
        nilai = str;
        return;
      }

      nilai=nilai+str;

      int intNilai=int.parse(nilai);

      if(intNilai>70) {
        passed_status = 'Passed';
        if(intNilai%5>2){
          intNilai=intNilai+(5-intNilai%5);//rounded up
          rounded_status='Rounded to '+intNilai.toString();
        }
      }else{
        passed_status = 'Not Passed';
      }


  });
}