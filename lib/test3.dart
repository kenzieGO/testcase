import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

String propic='https://png.pngtree.com/png-clipart/20201128/ourmid/pngtree-boy-playing-superman-png-image_2457157.jpg';


Future<String> getSharedPref() async {
  String props=propic;
  SharedPreferences prefs= await SharedPreferences.getInstance();
  if(prefs.containsKey('propic')) {
    props = prefs.getString('propic')!;
  }
  return props;
}


class test3 extends StatefulWidget{
  @override
  _test3State createState() => new _test3State();
}


class _test3State extends State<test3> with WidgetsBindingObserver {

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

      getSharedPref().then((pic) {
        if (propic != pic && pic!='') {
            setState(() {
            propic = pic;
          });
        }
      });


    return Scaffold(
        appBar: AppBar(
          title: new Text('Test 3'),
        ),
      body:Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  pushtoNext();
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child:Image.network(
                    propic,
                    fit:BoxFit.fill,
                    height: 80.0,
                    width: 80.0,
                  ),
                ),
              ),

              Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 0),
                      child: Text(
                        "Clark Kent",
                        style: TextStyle(fontSize:18.0,
                            color:  Color(0xFF000000),
                            fontWeight: FontWeight.w400,
                            fontFamily: "Roboto"),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 0),
                      child: Text(
                        "Superman",
                        style: TextStyle(fontSize:12.0,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w200,
                            fontFamily: "Roboto"),
                      ),
                    )
                  ]

              )
            ]

        ),
      )

    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(state==AppLifecycleState.resumed){
      getSharedPref().then((pic) {
        if (propic != pic && pic!='') {
          setState(() {
            propic = pic;
          });
        }
      });
    }
  }

  Future<void> pushtoNext() async {
    final pic=await Navigator.pushNamed(context, '/test3a',arguments: propic);
    if(pic!=null && pic!=propic){
      setState(() {
        propic = '$pic';
      });
    }
  }

}