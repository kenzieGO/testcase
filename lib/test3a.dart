
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:shared_preferences/shared_preferences.dart';
//import 'package:test1/utils.dart';

import 'models.dart';

var ip=ImagePicker();
var propic='https://64.media.tumblr.com/f4491633874acb1d4772454f0c10be6f/eb3029f28207f487-5c/s400x600/a79c6fc9e81287a89e0d79f0288271941c13db9d.jpg';
const double _maxHeight=512;
const double _maxWidth=512;

final ButtonStyle style_button=TextButton.styleFrom(
  primary: Colors.white,
  backgroundColor: Colors.blue,
  onSurface: Colors.grey,
  padding:  const EdgeInsets.all( 20.0),
);


class test3a extends StatefulWidget{
  @override
  _test3aState createState() => new _test3aState();
}

class _test3aState extends State<test3a> {

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    propic = ModalRoute.of(context)!.settings.arguments! as String;

    return Scaffold(
      appBar: AppBar(
        title: new Text('Profile Picker'),
      ),
      persistentFooterButtons: [Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextButton(key:null,
                  onPressed: () => getPicture(ImageSource.camera),
                  style: style_button,
                  child:
                  const Text(
                    "Camera",
                    style: TextStyle(fontSize:18.0,
                        color: Color(0xFFFFFFFF),
                        fontWeight: FontWeight.w200,
                        fontFamily: "Roboto"),
                  )
              ),
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                      TextButton(
                          key:null,
                          style: style_button,
                          onPressed: () => getPicture(ImageSource.gallery),
                          child:
                          const Text(
                            "Gallery",
                            style: TextStyle(fontSize:18.0,
                                color: Color(0xFFFFFFFF),
                                fontWeight: FontWeight.w200,
                                fontFamily: "Roboto"),
                          )
                      )
                )
            ]

        ),
      )],
      body:Padding(
        padding: EdgeInsets.all(8.0),
        child:
         Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.network(
                propic,
                fit:BoxFit.contain,
                height: 512,
                width: 512,
              ),


            ]

        ),
      )
    );
  }


  void getPicture(var from) async {
    XFile image = (await  ip.pickImage(
        source: from,
        imageQuality: 50,
        maxHeight: _maxHeight,
        maxWidth: _maxWidth,
    )) as XFile;

    setState(() {
      Navigator.pushNamed(context, '/test3b',arguments: args('propic',image.path));
    });

  }

}