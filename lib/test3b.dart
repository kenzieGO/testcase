import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/rendering.dart';
import 'package:test1/utils.dart';
import 'package:crop_image/crop_image.dart';
import 'package:flutter/material.dart';

import 'package:image/image.dart' as img;

import 'models.dart';

ui.Rect cropRect=const Rect.fromLTRB(0.1, 0.1, 0.9, 0.9);

final ButtonStyle style_button=TextButton.styleFrom(
  primary: Colors.white,
  backgroundColor: Colors.blue,
  onSurface: Colors.grey,
  padding:  const EdgeInsets.all( 20.0),
);


class test3b extends StatefulWidget{
  @override
  const test3b({Key? key}) : super(key: key);

  _test3bState createState() => new _test3bState();
}

class _test3bState extends State<test3b> with WidgetsBindingObserver{

  String propic='';

  var controller ;

  @override
  void dispose() {
    propic='';
    WidgetsBinding.instance!.removeObserver(this);
    //controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addObserver(this);
    controller = CropController(
              defaultCrop: cropRect,
            );

  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(state==AppLifecycleState.resumed){
      controller = CropController(
        defaultCrop: cropRect,
      );
    }
  }

  @override
  Widget build(BuildContext context) {

      var arg = ModalRoute
          .of(context)!
          .settings
          .arguments as args;

      propic = arg.message;

    //print(propic);

    return Scaffold(
      appBar: AppBar(
        title: new Text('Image Crop'),
      ),
      persistentFooterButtons: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextButton(
                    onPressed:_showDialog,
                    style: style_button,
                    child:
                    const Text(
                      "Crop Preview",
                      style: TextStyle(fontSize:18.0,
                          color: Color(0xFFFFFFFF),
                          fontWeight: FontWeight.w200,
                          fontFamily: "Roboto"),
                    )
                ),

                Padding(
                    padding: const EdgeInsets.all(8.0),
                      child:
                        TextButton(key:null,
                            onPressed:save,
                            style: style_button,
                            child:
                            const Text(
                              "Crop & Save",
                              style: TextStyle(fontSize:18.0,
                                  color: Color(0xFFFFFFFF),
                                  fontWeight: FontWeight.w200,
                                  fontFamily: "Roboto"),
                            )
                        ),
                  )
              ]

              )
          ),

      ],
      body:Padding(
        padding: EdgeInsets.all(8.0),
        child:
         Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              //RepaintBoundary(
                //key:cropshot,
                //child:
                CropImage(
                    controller: controller,
                    onCrop: on_crop,
                    image:
                        Image.file(
                          File(propic),
                          fit:BoxFit.contain,
                        )

                ),
              //),


            ]

        ),
      )
    );
  }

  Future<void> _showDialog() async {
    controller.croppedImage().then((image){
      showDialog<bool>(
        context: context,
        builder: (context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(8.0),
            titlePadding: EdgeInsets.all(8.0),
            title: const Text('Image Preview'),
            children: [
              image,
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    });

  }



  void save() async {

      utils.showLoaderDialog(context);

      var byteImage=File(propic).readAsBytesSync();
      var image = img.decodeImage(byteImage,)!;
      image = img.copyResize(image,width: 512);


      int left=(cropRect.left*image.width).toInt();
      int top=(cropRect.top*image.height).toInt();
      int width=(cropRect.width*image.width).toInt();
      int height=(cropRect.height*image.height).toInt();

      img.Image d=img.copyCrop(
          image,
          left,
          top,
          width,
          height
      );

      List<int> b = img.encodeJpg(d);

      utils.writeToFile(b).then((file){
        String auth='wTPb3I47TnooWoJijkUw65YIhp72X3YrE5fA+c27mZcJzEka6Uxp2jTV3qMabKESnxpFnARAWFE8NN79qcf3Dw==';
        String url='https://staging-satrio.kelaspintar.co.id/lpt-api/api/file';

        utils.DioUpload(context,url,auth,file.path);
      });


  }


  void on_crop(Rect value) {
    cropRect=value;
  }
}