import 'dart:convert';
import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test1/models.dart';

import 'package:path_provider/path_provider.dart';

class utils {

  static Future<File> writeToFile(List<int> data) async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/temp_profile.png';
    return File(filePath).writeAsBytes(data);
  }

  // content length Stream selalu tidak sama dengan yang file.length
  //TIDAK JADI PAKE INI

  static void Upload(BuildContext context,String file,String uploadURL,String auth,String method) async {

    var uri = Uri.parse(uploadURL);
    var request = http.MultipartRequest(method, uri);

    late var multipartFile;

    File imageFile=File(file);

    print(imageFile.path);

    multipartFile=await http.MultipartFile.fromPath('file', imageFile.path);

    request.files.add(multipartFile);

    int length=await imageFile.length();
    request.headers['Content-length'] = length.toString();
    request.headers['Content-Type']='multipart/form-data';
    request.headers['Connection']='keep-alive';
    request.headers['Authorization'] =auth;

    var response = await request.send();

    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) async {

      final dJSON=jsonDecode(value);

      if(dJSON['status']==1) {
        profilePic pic=profilePic.fromJson(dJSON['data']);

        print(pic.host+pic.path);
        await SharedPreferences.getInstance().then((prefs) {
          prefs.setString('propic',pic.host+pic.path);

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(dJSON['message'].toString()),
          ));

          Navigator.pop(context);
          Navigator.pop(context);

        });

      }


    });
  }

  static DioUpload(BuildContext context,String url,String auth,String filePath) async{

    String filename = filePath.split('/').last;

    var formData = FormData.fromMap({
      'file': await MultipartFile.fromFile(filePath, filename: filename)
    });

    var dio = Dio();
    dio.options.headers['Content-Type']='multipart/form-data';
    dio.options.headers['Connection']='keep-alive';
    dio.options.headers['Authorization'] =auth;

    Response response = await dio.put(url, data: formData);

    if(response.statusCode==200) {
      String msg = response.data['message'].toString();
      int status=response.data['status'];

      if(status==1) {
        profilePic pic = profilePic.fromJson(response.data['data']);

        print(pic.host+pic.path);
        await SharedPreferences.getInstance().then((prefs) {
          prefs.setString('propic',pic.host+pic.path);

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(msg),
          ));

          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pop(context,pic.host+pic.path);

        });

      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(msg),
        ));

        Navigator.pop(context);

      }

    }
  }

  static showLoaderDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
      content: Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 7),
              child:Text("Uploading..." )),
        ],),
    );

    showDialog(barrierDismissible: false,
      useRootNavigator: true,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }


}